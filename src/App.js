import avatar from "./assets/images/user.jpg";

function App() {
  return (
    <div style={{width: "800px", margin: "100px auto 0", textAlign: "center", border: "1px solid #ddd"}}>
      <div>
        <img src={avatar} alt="avatar" style={{borderRadius: "50%", width: "100px", marginTop: "-50px"}}/>
      </div>
      <div style={{fontSize: "18px", padding: "5px 0"}}>
        <p>This is one of the best developer blogs on the planet! I read it daily to improve my skills.</p>
      </div>
      <div style={{fontSize: "15px", marginBottom: "30px"}}>
        <p><b>Tammy Stevens</b>&nbsp; * &nbsp;Front End Developer</p>
      </div>
    </div>
  );
}

export default App;
